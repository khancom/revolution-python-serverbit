from tornado import websocket, web, ioloop
import _thread
import json
import signal
import sys, traceback, os
from bitalino import *
from os.path import expanduser

### WP edit
from BitalinoOSC import *
import fileinput, time
import deviceFinder as deviceFinder
from ServerGUI import *
from reset import reset_config, rerun
#import glob

device = None
USE_STDIO = False
USE_GUI = True
RUNNING = True
OS = None

json_file_path = "config.json"
default_addr = "WINDOWS/LINUX - XX:XX:XX:XX:XX:XX | MAC - /dev/tty.BITalino-XX-XX-DevB"
cl = []

def tostring(data):
    """
    :param data: object to be converted into a JSON-compatible `str`
    :type data: any
    :return: JSON-compatible `str` version of `data`

    Converts `data` from its native data type to a JSON-compatible `str`.
    """
    dtype = type(data).__name__
    if dtype == 'ndarray':
        if numpy.shape(data) != ():
            data = data.tolist()  # data=list(data)
        else:
            data = '"' + data.tostring() + '"'
    elif dtype == 'dict' or dtype == 'tuple':
        try:
            data = json.dumps(data)
        except:
            pass
    elif dtype == 'NoneType':
        data = ''
    elif dtype == 'str' or dtype == 'unicode':
        data = json.dumps(data)

    return str(data)


class SocketHandler(websocket.WebSocketHandler):
    def check_origin(self, origin):
        return True

    def open(self):
        if self not in cl:
            cl.append(self)
        print("CONNECTED")

    def on_message(self, message):
        self.write_message(u"You said: " + message)

    def on_close(self):
        if self in cl:
            cl.remove(self)
        print("DISCONNECTED")

def change_json_value(file,orig,new):
    ##find and replace string in file, keeps formatting
    print(file, orig, new)
    for line in fileinput.input(file, inplace=1):
        if orig in line:
            line = line.replace(orig,new)
        sys.stdout.write(line)

def save_config():
    for key, old_value in gui.config_data.items():
        format = str('"' + key + '": ')
        old_value = format + str(old_value)
        new_value = format + str(gui.get_text_inputs()[key])
        if new_value not in old_value:
            print ("writing to json:" + new_value)
            change_json_value(json_file_path, old_value, str(new_value))

def signal_handler(signal, frame):
    print('TERMINATED')
    sys.exit(0)

def listDevices():
    print ("============")
    print ("please select your device:")
    print ("Example: /dev/tty.BITalino-XX-XX-DevB")
    allDevices = deviceFinder.findDevices(OS)
    return allDevices

# def save_default_device(query, d):
#     while "please inset y or n":
#         print (query)
#         reply = sys.stdin.readline()
#         #reply = str(input(query + ' (y/n): ')).lower().strip()
#         if reply[:1] == 'y':
#             ##find and replace string in file, keeps formatting
#             save_device(d, json_file_path)
#             return
#         if reply[:1] == 'n':
#             return

# def selectDevice():
#     print ('searching for devices...')
#     try:
#         allDevices = discover_devices(lookup_names=True)
#         numDevices = len(allDevices)
#     except (BluetoothError, OSError) as e:
#         print (e)
#         print("+++++")
#         print ("Please check bluetooth is turned on and try again")
#         sys.exit(1)
#     print ("============")
#     print ("found %d devices" % numDevices)
#     print ("please select a PLUX device from the list below or press ENTER to redo search:")
#     count = 1
#     for name, addr in allDevices:
#         print (str(count) + ". " + " %s - %s" % (addr, name))
#         count = count + 1
#     try:
#         deviceID = int(input("please enter your device number (1 - %d): " % numDevices))
#         if 1 <= deviceID <= numDevices:
#             selectedDeviceAddr, selectedDeviceName = allDevices[deviceID - 1]
#             print("connecting to %s" % selectedDeviceAddr)
#             print("device: %s" % deviceFinder.check_type(selectedDeviceName))
#             return selectedDeviceAddr
#         else:
#             raise ValueError()
#     except (TypeError, ValueError, IndexError) as e:
#         print("device number not in range")
#         return None

def check_device_addr(addr):
    new_device = None
    if default_addr in addr:
        text_out ("device address has not been changed in config.json" + "\n" +
                    "please select a PLUX device")
        gui.show_devices( listDevices() )
        while new_device is None:
            #new_device = selectDevice()
            new_device = gui.selectedDeviceAddr
        if gui.write_device:
            text_out("saving %s to config.json" % new_device)
            change_json_value(json_file_path,default_addr,new_device)
        return new_device
    else:
        text_out ("connecting to %s ..." % addr)
    return addr

def BITalino_handler(mac_addr, ch_mask, srate, nsamples, labels, mode, OSC_out):
    new_mac_addr = check_device_addr(mac_addr)
    ch_mask = numpy.array(ch_mask) - 1
    try:
        device = BITalino(new_mac_addr)
        text_out("reading inputs from %s" % new_mac_addr)
        print(ch_mask)
        print(srate)
        device.start(srate, ch_mask)
        cols = numpy.arange(len(ch_mask) + 5)
        text_out("sending %i channels to %s:%i \n OSC_address: %s"
                    % (len(labels), net_config.ip,net_config.port, OSC_out.output_address))
        gui.reset_button.configure(text="Close ServerBIT", command=close)

        while (1):
            data = device.read(nsamples)
            res = "{"
            for i in cols:
                idx = i
                if (i > 4): idx = ch_mask[i - 5] + 5
                res += '"' + labels[idx] + '":' + tostring(data[:, i]) + ','
            res = res[:-1] + "}"
            if len(cl) > 0: cl[-1].write_message(res)
            if 'OSC' in mode:
                OSC_out.output_bundle(res)
                #OSC_out.output_labelled(res)
    except Exception as e:
        ex = str(e)
        text_out(ex)
        time.sleep(1)
        reset_config(json_file_path, gui.ask_reset(
            'Unable to connect to this device, restart ServerBIT?', ex))

def disconnect_device():
    if device is not None:
        # Stop acquisition
        device.stop()
        # Close connection
        device.close()

def reset():
    disconnect_device()
    if gui.ask_to_save():
        save_config()
    rerun()

def close():
    disconnect_device()
    if gui.ask_to_save():
        save_config()
    sys.exit(1)

def text_out(str):
    print(str)
    gui.change_label(str)

app = web.Application([(r'/', SocketHandler)])

if __name__ == '__main__':
    OS = platform.system()
    print ("Detected platform: " + OS)
    gui = ServerGUI()
    gui.wm_title("ServerBIT")
    if OS == "Windows":
        print("nah")
        gui.wm_iconbitmap("bin/BITalino.ico")
    else:
        print("yeah")
        gui.wm_iconbitmap("bin/BITalino.incs")
    gui.protocol("WM_DELETE_WINDOW", close)
    gui.reset_button.configure(command=reset)

    home = expanduser("~") + '/ServerBIT'
    print(home)
    try:
        with open(home + '/config.json') as data_file:
            config = json.load(data_file)
            json_file_path = home + '/config.json'
        # with open('config.json') as data_file:
        #     config = json.load(data_file)
    except:
        try:
            with open('config.json') as data_file:
                config = json.load(data_file)
                json_file_path = home + '/config.json'
            os.mkdir(home)
            with open(home + '/config.json', 'w') as outfile:
                json.dump(config, outfile)
            for file in ['ClientBIT.html', 'jquery.flot.js', 'jquery.js']:
                with open(home + '/' + file, 'w') as outfile:
                    outfile.write(open(file).read())
        except:
            if gui.ask_reset("configuration error: restart ServerBIT using default settings?", "error reading json file"):
                disconnect_device()
                reset_config(json_file_path, True)
            else:
                close()

    net_config = Net_Config(config['network'])
    protocol = net_config.protocol

    gui.show_config(config)

    if 'WebSockets' in net_config.protocol:
        signal.signal(signal.SIGINT, signal_handler)
        app.listen(config['port'])
        print('LISTENING')
    elif 'OSC' in net_config.protocol:
        OSC_enabled = True
        bit_OSC = Bitalino_OSC(net_config, config['labels'], 0)
    else:
        print ("No valid input for WebSockets_OSC in config.json")
        print ("please add 'OSC' or 'WebSockets' to file and try again")
        sys.exit(1)
    _thread.start_new_thread(BITalino_handler,
                             (config['device'], config['channels'], config['sampling_rate'], config['buffer_size'], config['labels'], protocol, bit_OSC))
    gui.mainloop()
    ioloop.IOLoop.instance().start()
