### WP edit
'''
requires python-osc and python3x
pip3 install python-osc
'''
import json
from dsp import *
from pythonosc import osc_message_builder
from pythonosc import osc_bundle_builder
from pythonosc import udp_client

from pythonosc import dispatcher, osc_server
from time import sleep


class Net_Config:
    ip = ""
    port = 8080
    protocol = "OSC"

    def __init__(self, netConfig):
        self.ip = netConfig["ip"]
        self.port = netConfig["port"]
        self.protocol = netConfig["WebSockets_OSC"]

class Bitalino_OSC:
    device_name = "/bitalino"
    device_number = str(0)
    data_feature = "/raw/"
    output_address = ""
    labels = []

    BITalino_device = None

    def __init__(self, netConfig, lbs, n):
        self.ip, self.port = netConfig.ip, netConfig.port
        self.client = udp_client.SimpleUDPClient(netConfig.ip, netConfig.port)
        self.labels = lbs
        self.device_number = str(n)
        self.output_address = "/" + self.device_number + "/bitalino"

    def init_server(self):
        def osc_handler(args, name, *trigger_values):
            led = 1 if trigger_values[0] > 0.5 else 0
            buzzer = 1 if trigger_values[1] > 0.5 else 0
            pwd = trigger_values[2]
            self.BITalino_device.trigger([led, buzzer])
            # self.bq.put([name[0], value])

        while self.BITalino_device is None:
            sleep(0.1)
        d = dispatcher.Dispatcher()
        d.map("/0/trigger", osc_handler, 'triggers')
        self.server = osc_server.ThreadingOSCUDPServer(
              (self.ip, 8888), d)
        print("Serving on {}".format(self.server.server_address))
        self.server.serve_forever()

    def sendTestBundle(self, numOutputs):
        bundle = osc_bundle_builder.OscBundleBuilder(
            osc_bundle_builder.IMMEDIATELY)
        msg = osc_message_builder.OscMessageBuilder(
            address=self.output_address)
        # Test bundle outputs: 1, 2, 3, ...
        testVal = 1.0
        for i in range(numOutputs):
            msg.add_arg(testVal)
            testVal = testVal + 1.0
        bundle.add_content(msg.build())
        bundle = bundle.build()
        self.client.send(bundle)

    def output_bundle(self, data):
        ch = ["'A1'", "'A2'", "'A3'", "'A4'", "'A5'", "'A6'"]
        data = json.loads(data)
        # printJSON(data)
        bundle = osc_bundle_builder.OscBundleBuilder(
            osc_bundle_builder.IMMEDIATELY)
        msg = osc_message_builder.OscMessageBuilder(
            address=self.output_address)
        for label in self.labels:
            #print (label + " " + str(data[label][0]))
            msg.add_arg(data[label][0])
        bundle.add_content(msg.build())
        bundle = bundle.build()
        self.client.send(bundle)

    def output_filtered(self, data):
        ch = ["'A1'", "'A2'", "'A3'", "'A4'", "'A5'", "'A6'"]
        data = json.loads(data)
        # printJSON(data)
        bundle = osc_bundle_builder.OscBundleBuilder(
            osc_bundle_builder.IMMEDIATELY)
        msg = osc_message_builder.OscMessageBuilder(
            address=self.output_address)
        for label in self.labels:
            if 'A1' in label:
                msg.add_arg(avg(data[label]))
            else:
                msg.add_arg(data[label][0])
        bundle.add_content(msg.build())
        bundle = bundle.build()
        self.client.send(bundle)

    def output_labelled(self, data):
        ch = ["'A1'", "'A2'", "'A3'", "'A4'", "'A5'", "'A6'"]
        data = json.loads(data)
        # printJSON(data)
        bundle = osc_bundle_builder.OscBundleBuilder(
            osc_bundle_builder.IMMEDIATELY)

        for label in self.labels:
            self.output_address += self.device_number + "/bitalino/" + label
            msg = osc_message_builder.OscMessageBuilder(
                address=output_address)
            msg.add_arg(data[label][0])
            msg.build()
            self.client.send(msg)

def printJSON(decoded_json_input):
    try:
        # pretty printing of json-formatted string
        print (json.dumps(decoded_json_input, sort_keys=True , indent=4))
    except (ValueError, KeyError, TypeError):
        print ("JSON format error")
