import matplotlib
matplotlib.use("TkAgg")
from matplotlib import pylab
from pylab import *
from numpy import *
from scipy import signal

def lowpass(s, f, order=2, fs=1000.0):
    b, a = signal.butter(order, f / (fs/2))
    return signal.lfilter(b, a, s)

def avg(*s):
    s = array(s)
    emg_data = s
    abs_data = abs(emg_data-mean(emg_data))
    return lowpass(abs_data, 5).item(0) # filter with a lowpass filter at 10Hz
