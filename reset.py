from shutil import copyfile
import time, os, sys

def reset_config(json_file_path, restart_program):
    copyfile("bin/clean_config.json", 'config.json')
    copyfile("bin/clean_config.json", json_file_path)
    time.sleep(2)
    print("Restored config.json")
    print("QUITAPP\n")
    if restart_program:
        os.execl(sys.executable, sys.executable, *sys.argv)
    else:
        os._exit(0)

def rerun():
    time.sleep(0.5)
    print("QUITAPP\n")
    os.execl(sys.executable, sys.executable, *sys.argv)

if __name__ == "__main__":
    reset_config(False)
