import asyncio
import websockets
import _thread
import json
import signal as sig
import sys, traceback, os
from bitalino import *
from os.path import expanduser

### WP edit
import argparse
from BitalinoOSC import *
import fileinput, time
import deviceFinder as deviceFinder
from ServerGUI import *
from reset import reset_config, rerun
from multiprocessing import Process

USE_STDIO = False
RUNNING = True
OS = None

json_file_path = "config.json"
default_addr = "WINDOWS/LINUX - XX:XX:XX:XX:XX:XX | MAC - /dev/tty.BITalino-XX-XX-DevB"
cl = []

class Utils:
    BITalino_device = None
    USE_GUI = True
    sensor_data_json = ""
    labels = ["nSeq", "I1", "I2", "O1", "O2","A1","A2","A3","A4","A5","A6"]

ut = Utils()

def tostring(data):
    """
    :param data: object to be converted into a JSON-compatible `str`
    :type data: any
    :return: JSON-compatible `str` version of `data`

    Converts `data` from its native data type to a JSON-compatible `str`.
    """
    dtype = type(data).__name__
    if dtype == 'ndarray':
        if numpy.shape(data) != ():
            data = data.tolist()  # data=list(data)
        else:
            data = '"' + data.tostring() + '"'
    elif dtype == 'dict' or dtype == 'tuple':
        try:
            data = json.dumps(data)
        except:
            pass
    elif dtype == 'NoneType':
        data = ''
    elif dtype == 'str' or dtype == 'unicode':
        data = json.dumps(data)

    return str(data)


# class SocketHandler(websocket.WebSocketHandler):
#     def check_origin(self, origin):
#         return True
#
#     def open(self):
#         if self not in cl:
#             cl.append(self)
#         print("CONNECTED")
#
#     def on_message(self, message):
#         self.write_message(u"You said: " + message)
#
#     def on_close(self):
#         if self in cl:
#             cl.remove(self)
#         print("DISCONNECTED")

def change_json_value(file,orig,new):
    ##find and replace string in file, keeps formatting
    ##print(file, orig, new)
    for line in fileinput.input(file, inplace=1):
        if orig in line:
            line = line.replace(orig,new)
        sys.stdout.write(line)

def save_config():
    for key, old_value in gui.config_data.items():
        if "WebSockets_OSC" in key:
            old_value = '"{0}"'.format(old_value)
        format = str('"' + key + '": ')
        old_value = format + str(old_value)
        new_value = format + str(gui.get_text_inputs()[key])
        if new_value not in old_value:
            print (old_value)
            print ("writing to json:" + new_value)
            change_json_value(json_file_path, old_value, str(new_value))

def signal_handler(signal, frame):
    print('TERMINATED')
    sys.exit(0)

def listDevices():
    print ("============")
    print ("please select your device:")
    print ("Example: /dev/tty.BITalino-XX-XX-DevB")
    allDevices = deviceFinder.findDevices(OS)
    return allDevices

def check_device_addr(addr):
    new_device = None
    if default_addr in addr:
        text_out ("device address has not been changed in config.json" + "\n" +
                    "please select a PLUX device")
        gui.show_devices( listDevices() )
        while new_device is None:
            new_device = gui.selectedDeviceAddr
        addr = new_device
        if gui.write_device:
            text_out("saving %s to config.json" % new_device)
            change_json_value(json_file_path,default_addr,new_device)
            time.sleep(1)
    text_out ("connecting to %s ..." % addr)
    return addr

def BITalino_handler(mac_addr, ch_mask, srate, nsamples, labels, OSC_mode, OSC_out):
    new_mac_addr = check_device_addr(mac_addr)
    ch_mask = numpy.array(ch_mask) - 1
    if not OSC_mode:
        # labels = [l.replace("'", "") for l in labels]
        labels = ["nSeq", "I1", "I2", "O1", "O2","A1","A2","A3","A4","A5","A6"]
    try:
        device = BITalino(new_mac_addr)
        ut.BITalino_device = device
        if OSC_mode:
            bit_OSC.BITalino_device = device
        text_out("reading inputs from %s" % new_mac_addr)
        print(ch_mask, srate)
        device.start(srate, ch_mask)
        device.trigger([1, 0])
        cols = numpy.arange(len(ch_mask) + 5)
        if ut.USE_GUI: gui.reset_button.configure(text="Close ServerBIT", command=close)
        if not OSC_mode:
            text_out("sending %i channels to %s:%i \n open ClientBIT.html (GUI no longer active)"
                        % (len(labels), net_config.ip,net_config.port))
            time.sleep(0.5)
            gui.quit()
        if OSC_mode:
            text_out("sending %i channels to %s:%i \n OSC_address: %s"
                        % (len(labels), net_config.ip,net_config.port, OSC_out.output_address))

        while (1):
            data = device.read(nsamples)
            res = "{"
            for i in cols:
                idx = i
                if (i > 4): idx = ch_mask[i - 5] + 5
                res += '"' + labels[idx] + '":' + tostring(data[:, i]) + ','
            res = res[:-1] + "}"
            ut.sensor_data_json = res

            if OSC_mode:
                #OSC_out.output_bundle(res)
                OSC_out.output_filtered(res)
                #OSC_out.output_labelled(res)
    except Exception as e:
        ex = str(e)
        text_out(ex)
        time.sleep(1)
        if ut.USE_GUI:  reset_config(json_file_path, gui.ask_reset(
            'Unable to connect to this device, restart ServerBIT?', ex))

def disconnect_device():
    device = ut.BITalino_device
    if device is not None:
        device.trigger([0, 0])
        # Stop acquisition
        device.stop()
        # Close connection
        device.close()

def reset():
    if gui.ask_to_save():
        save_config()
    rerun()

def close():
    if gui.ask_to_save():
        save_config()
    gui.destroy()
    sys.exit(1)

def text_out(str):
    print(str)
    if ut.USE_GUI:
        gui.change_label(str)

async def webApp(ws, path):
    print('LISTENING')
    while True:
        await ws.send(ut.sensor_data_json)
        await asyncio.sleep(0.1)

if __name__ == '__main__':
    OS = platform.system()
    print ("Detected platform: " + OS)

    home = expanduser("~") + '/ServerBIT'
    print(home)
    try:
        with open(home + '/config.json') as data_file:
            config = json.load(data_file)
            json_file_path = home + '/config.json'
        # with open('config.json') as data_file:
        #     config = json.load(data_file)
    except:
        try:
            with open('config.json') as data_file:
                config = json.load(data_file)
                json_file_path = home + '/config.json'
            os.mkdir(home)
            with open(home + '/config.json', 'w') as outfile:
                json.dump(config, outfile)
            for file in ['ClientBIT.html', 'jquery.flot.js', 'jquery.js']:
                with open(home + '/' + file, 'w') as outfile:
                    outfile.write(open(file).read())
        except:
            reset_config(json_file_path, True)

    net_config = Net_Config(config['network'])
    OSC_enabled = False
    bit_OSC = Bitalino_OSC(net_config, config['labels'], 0)

    if 'OSC' in net_config.protocol:
        OSC_enabled = True

    if ut.USE_GUI:
        gui = ServerGUI()
        gui.wm_title("ServerBIT")
        if OS == "Windows":
            gui.wm_iconbitmap("bin/BITalino.ico")
        gui.protocol("WM_DELETE_WINDOW", close)
        gui.reset_button.configure(command=reset)
        gui.show_config(config)

    try:
        _thread.start_new_thread(BITalino_handler,
                                 (config['device'], config['channels'], config['sampling_rate'], config['buffer_size'], config['labels'], OSC_enabled, bit_OSC))

        if ut.USE_GUI:
            gui.mainloop()

        if 'WebSockets' in net_config.protocol:
            start_server = websockets.serve(webApp, '127.0.0.1', 9001)
            asyncio.get_event_loop().run_until_complete(start_server)
            asyncio.get_event_loop().run_forever()

    # else:
    #     _thread.start_new_thread(bit_OSC.init_server())
    finally:
        disconnect_device()
        sys.exit(1)
