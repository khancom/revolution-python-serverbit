import tkinter as tk
import tkinter.messagebox
from flatten_dict import flatten
import time, os, sys
from reset import rerun

def remove_parent(k1, k2):
    return k2

def add_quote(a):
    return '"{0}"'.format(a)

class ServerGUI(tk.Tk):
    selectedDeviceAddr = None
    write_device = True
    entry_fields = []
    entry_inputs = {}
    show_entry_fields = False
    clicked_entry_fields = False

    def __init__(self, *args, **kwargs):
        tk.Tk.__init__(self, *args, **kwargs)

        label = "please select a PLUX device"
        self.label = tk.Label(self, text=label, width=40, height=2)
        self.label.pack(side="bottom", fill="both", padx=100, pady=5)

        self.reset_button = tk.Button(self, text = "Restart ServerBIT")
        self.reset_button.pack(side="bottom")

        button = tk.Button(self, text = "Show/Hide Options")
        button.bind('<Button-1>', self.hide_options)
        button.pack(side="bottom")

    def show_config(self, config_data):
        config_data = flatten(config_data, reducer=remove_parent)
        for key, value in config_data.items():
            self.entry_inputs[key] = value
            self.make_input(key, value)
            self.config_data = self.entry_inputs.copy()

    def show_devices(self, allDevices):
        devicesLabel = tk.Label(self, text="Devices", width=40, height=2)
        devicesLabel.pack(side="top", fill="y", padx=100, pady=0)
        lb = tk.Listbox(self, height=len(allDevices)+1, bd=1)
        for dev in allDevices:
            str = '%s (%s)' % (dev[0], dev[1])
            lb.insert("end", str)
            #lb.insert("end", dev[0])
        lb.bind("<Double-Button-1>", self.OnDouble)
        lb.pack(side="top", fill="both", expand=True, padx=5)

    def hide_options(self, event):
        self.show_entry_fields = not self.show_entry_fields
        for ef in self.entry_fields:
            if self.show_entry_fields:
                ef.pack(side = "top", fill="both", expand=True, padx=50)
            else:
                ef.pack_forget()

    def OnClickEntry(self, event):
        self.clicked_entry_fields = True
        self.get_text_inputs()

    def OnDouble(self, event):
        widget = event.widget
        selection = widget.curselection()
        selection = widget.get(selection[0])
        selection = selection.split('(')[0] #get the mac address
        selection = selection.strip() #remove spaces
        self.device_entry.delete(0, "end")
        self.device_entry.insert("end", selection)
        self.label.configure(text="make %s the default device?" % selection);
        write_device = tkinter.messagebox.askyesno('Save Device',
                                  ('make %s the default device?\n\n' % selection))
        self.selectedDeviceAddr = selection
        self.write_device = write_device
        return

    def dropdown_input(self, choices, input):
        tkvar = tk.StringVar(self)
        tkvar.set(input) # set the default option
        menu = tk.OptionMenu(self, tkvar, *choices, command=self.OnClickEntry)
        menu.list = tkvar
        return menu

    def make_input(self, name, input):
        L1 = tk.Label(self, text=name)
        E1 = tk.Entry(self, bd=2)
        E1.insert("end", str(input))
        E1.bind('<Button-1>', self.OnClickEntry)

        if "device" in name:
            self.device_entry = E1

        if "sampling_rate" in name:
            menu = self.dropdown_input([1000, 100, 10, 1], input)
            E1 = menu

        if "WebSockets_OSC" in name:
            menu = self.dropdown_input(['"WebSockets"', '"OSC"'], add_quote(input))
            E1 = menu

        if "channels" in name or "labels" in name:
            E1.configure(state="disabled")

        self.entry_fields.extend( [L1, E1] )

    def change_label(self, str):
        self.label.configure(text=str)

    def get_text_inputs(self):
        key = value = ""
        for ef in self.entry_fields:
            if isinstance(ef, tk.Label):
                key = ef["text"]
            elif isinstance(ef, tk.Entry):
                value = ef.get()
            elif isinstance(ef, tk.OptionMenu):
                value = ef.list.get()
            self.entry_inputs[key] = value
            #print(self.entry_inputs)
        return self.entry_inputs

    def ask_to_save(self):
        save_to_json = False
        if self.clicked_entry_fields:
            save_to_json = tkinter.messagebox.askyesno('Restart ServerBIT',
                                    'Save settings to config.json?')
        return save_to_json

    def ask_reset(self, label_text, error_message):
        self.change_label(error_message)
        restart = tkinter.messagebox.askyesno('Device Error', label_text)
        return restart

    def deleteThis(self):
        self.destroy()
        
    def on_closing(self):
        self.destroy()
        print("QUITAPP\n")
